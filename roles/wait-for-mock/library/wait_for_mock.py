#!/usr/bin/env python3
# Copyright 2022 Red Hat
# SPDX-License-Identifier: Apache-2.0

import traceback
import json
import time
from urllib import request
import argparse
import sys


def info(logger, msg):
    if logger is None:
        print(msg, file=sys.stderr)
    else:
        logger.append(msg)


def json_open(logger, url):
    info(logger, "Checking %s" % url)
    req = request.urlopen(url)
    return json.loads(req.read().decode("utf-8"))


def from_buildset(logger, buildset):
    for change in buildset:
        if change["live"] is not True:
            continue
        for job in change["jobs"]:
            if job["name"] != "mock-build":
                continue
            if job.get("report_url", "").startswith("http"):
                return job["report_url"]
            else:
                return None


def from_builds(logger, builds):
    if not builds:
        raise RuntimeError("Build does not exists")

    return builds[0]["log_url"]


def keep_check_pipeline(logger, change):
    for job in change.get("jobs", []):
        if job.get("pipeline") == "check":
            return True
        return False


def find_logurl(logger, zuul_api_url, zuul_change):
    start_time = time.monotonic()
    base_url = zuul_api_url.rstrip("/")
    while True:
        buildset = list(
            filter(
                lambda change: keep_check_pipeline(logger, change),
                json_open(logger, base_url + "/status/change/" + zuul_change),
            )
        )
        if buildset:
            # Checks are still running, look for the mock-build log_url
            logurl = from_buildset(logger, buildset)
        else:
            # Checks are already completed, look in the completed builds
            builds = json_open(
                logger, base_url + "/builds?job_name=mock-build&change=" + zuul_change
            )
            logurl = from_builds(logger, builds)

        if logurl:
            return logurl

        if time.monotonic() - start_time > 32400:
            raise RuntimeError("Timeout")

        time.sleep(30)


params = dict(
    zuul_api_url=dict(required=True, type="str"),
    zuul_change=dict(required=True, type="str"),
)


def ansible_main():
    from ansible.module_utils.basic import AnsibleModule

    module = AnsibleModule(argument_spec=params)

    p = module.params
    try:
        logs = []
        result = find_logurl(logs, p.get("zuul_api_url"), p.get("zuul_change"))
        module.exit_json(changed=False, url=result, logs=logs)
    except RuntimeError as e:
        module.fail_json(msg=str(e))
    except Exception as e:
        log = [str(e), traceback.format_exc()]
        module.fail_json(msg=str(e), log="\n".join(log))


def cli_main():
    parser = argparse.ArgumentParser(description="Wait for a mock build")
    for arg in params.keys():
        parser.add_argument("--" + arg, required=True)
    args = parser.parse_args()

    print(find_logurl(None, args.zuul_api_url, args.zuul_change))


if __name__ == "__main__":
    if sys.stdin.isatty():
        cli_main()
    else:
        ansible_main()
